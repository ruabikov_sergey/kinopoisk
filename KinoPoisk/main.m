//
//  main.m
//  KinoPoisk
//
//  Created by user on 07.11.16.
//  Copyright © 2016 Ruabikov Sergey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
