//
//  FirstViewController.m
//  KinoPoisk
//
//  Created by user on 07.11.16.
//  Copyright © 2016 Ruabikov Sergey. All rights reserved.
//

#import "MovieListViewController.h"
#import "NetController.h"
#import "MovieData.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "DetailViewController.h"
#import "Utils.h"



#define MOVIE_CELL_ID  @"MovieCell"
#define INFO_CELL_ID   @"InfoCell"

#define OPTION_CITY   0
#define OPTION_DATE   1
#define OPTION_GENRE  2

#define ORDER_RATING  0
#define ORDER_ALPHA   1



@interface MovieListViewController ()
{
    NSURLRequest *imageRequest;
    NSArray* listOptionsKeys;
    NSMutableDictionary* optionsKeys;
    NSArray* genreArray;
    NSArray* cityArray;
   
}
@end

@implementation MovieListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.viewPicker setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
    [self showPicker:NO];
    
    [self initTableData];
    
    [self setSelectDate:[[Utils sharedClient]formatDate:[NSDate date]]];
    [self setCityID:@"490"];
    
    [self getMovieListFromCity];
    [self initPicker];
  
   
    

}

- (void)initTableData{
    self.movieListArray    = [[NSMutableArray alloc]init];
    self.movieListArrayAll = [[NSMutableArray alloc]init];
    self.dateArray = [[NSMutableArray alloc]init];
    listOptionsKeys = [[NSArray alloc] initWithObjects: @"Город", @"Дата", @"Жанр", nil];
    optionsKeys = [[NSMutableDictionary alloc] init];
    [optionsKeys setObject: @"Калининград" forKey: listOptionsKeys[0]];
    [optionsKeys setObject: @"Все"  forKey: listOptionsKeys[1]];
    [optionsKeys setObject: @"Все жанры"      forKey: listOptionsKeys[2]];
    
    
    genreArray  = [[NSArray alloc]initWithObjects:@"Все жанры",@"боевик",@"детектив",@"драма",@"комедия",@"криминал",@"мультфильм",@"мюзикл",@"приключения",@"семейный",@"триллер",@"ужасы",@"фантастика",@"фэнтези",nil];
    
    cityArray  = [[NSArray alloc]initWithObjects:@"Москва",@"Санкт-Петербург",@"Калининград",@"Сочи",nil];
    
    [self setPickerArray:self.dateArray];
    
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor lightGrayColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(getMovieListFromCity)
                  forControlEvents:UIControlEventValueChanged];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                forKey:NSForegroundColorAttributeName];
    NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:@"Обновление..." attributes:attrsDictionary];
    
    
    self.refreshControl.attributedTitle = attributedTitle;
    [self.tableView addSubview:self.refreshControl];
    
}
- (void)getUpdateMovieTable{
    
}


- (void)loadTestMovieData{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"getSoonFilms" ofType:@"txt"];
    NSString *content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSError *err = nil;
    NSDictionary *dict =
    [NSJSONSerialization JSONObjectWithData:[content dataUsingEncoding:NSUTF8StringEncoding]
                                    options:NSJSONReadingMutableContainers
                                      error:&err];
    
    [self setupMovieList:dict];
}



- (void)getMovieListFromCity{
    
    NSMutableDictionary* dicParams = [[NSMutableDictionary alloc]init];
    //[dicParams setObject: [self getSelectDate] forKey:@"date"];
    [dicParams setObject: @"10.11.2016" forKey:@"date"];
    
    //[dicParams setObject: [self getCityID]     forKey:@"cityID"];
    
    [[NetController sharedClient] request: RENT_MOVIES parameters:dicParams success:^(NSURLSessionTask *task, id responseObject) {
        [self setupMovieList:responseObject];
    }failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [[Utils sharedClient]showMessage:@"Будут загружены тестовые данные." withTitle:@"Ошибка Сервера" inView:self.mainView];
        [self loadTestMovieData];
    }];
    
    

}
- (void)setupMovieList:(NSDictionary*)dict{
        NSMutableOrderedSet* orderedSetDate = [[NSMutableOrderedSet alloc]init];
        [self.movieListArray removeAllObjects];
        [self.movieListArrayAll removeAllObjects];
        [self.dateArray removeAllObjects];
    
        NSArray *movieArray   = [dict objectForKey:@"previewFilms"];
        
        for (NSArray* movieArray2 in movieArray) {
         for (NSDictionary* dicItem in movieArray2) {
        
         MovieData* mdata = [[MovieData alloc]init];
         mdata.Id         = [dicItem objectForKey:@"id"];
         mdata.nameRu     = [dicItem objectForKey:@"nameRU"];
         mdata.nameEn     = [dicItem objectForKey:@"nameEN"];
         mdata.year       = [dicItem objectForKey:@"year"];
         mdata.rating     = [dicItem objectForKey:@"rating"];
         mdata.posterURL  = [dicItem objectForKey:@"posterURL"];
         mdata.filmLength = [dicItem objectForKey:@"filmLength"];
         mdata.country    = [dicItem objectForKey:@"country"];
         mdata.genre      = [dicItem objectForKey:@"genre"];
         mdata.videoURL   = [dicItem objectForKey:@"videoURL"];
         mdata.premiereRU = [dicItem objectForKey:@"premiereRU"];
        [orderedSetDate addObject:mdata.premiereRU];
        [self.movieListArray addObject:mdata];
        }
       }
        [self.movieListArrayAll addObjectsFromArray:self.movieListArray];
        [self.dateArray addObjectsFromArray:[orderedSetDate array]];
        [self.dateArray insertObject:@"Все" atIndex:0];
        [self.tableView reloadData];
        [self orderMovieListTo:ORDER_RATING];
    [self.refreshControl endRefreshing];

}
- (void)getCinemasFromCity{
    
    NSMutableDictionary* dicParams = [[NSMutableDictionary alloc]init];
    [dicParams setObject: [self getSelectDate] forKey:@"date"];
    [dicParams setObject: [self getCityID] forKey:@"cityID"];
    
    [[NetController sharedClient] request: RENT_MOVIES parameters:dicParams success:^(NSURLSessionTask *task, id responseObject) {
        [self setupMovieList:responseObject];
    }failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [[Utils sharedClient]showMessage:@"Будут загружены тестовые данные." withTitle:@"Ошибка Сервера" inView:self.mainView];
        [self loadTestMovieData];
    }];
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0) return 3;
    else return [self.movieListArray count];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section == 1) return @"";
    else return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   if(indexPath.section == 1) return 80;
   else return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* cellID ;
    if((indexPath.section==0)) cellID = INFO_CELL_ID; else cellID = MOVIE_CELL_ID;
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
  
    if((indexPath.section==0)){ //Options Row
        UILabel *label1 = (UILabel *)[cell viewWithTag:201];
         label1.text = @"City";
        ((UILabel *)[cell viewWithTag:202]).text = [optionsKeys objectForKey: [listOptionsKeys objectAtIndex:indexPath.row]];
        ((UILabel *)[cell viewWithTag:201]).text = [listOptionsKeys objectAtIndex:indexPath.row];
        
    }else{ //MovieList
    NSString* name = [[self.movieListArray objectAtIndex:indexPath.row] NameRu];
    ((UILabel *)[cell viewWithTag:101]).text = [[self.movieListArray objectAtIndex:indexPath.row] NameRu];
    ((UILabel *)[cell viewWithTag:102]).text = [[self.movieListArray objectAtIndex:indexPath.row] NameEn];
    ((UILabel *)[cell viewWithTag:103]).text = [[self.movieListArray objectAtIndex:indexPath.row] Rating];
    ((UILabel *)[cell viewWithTag:104]).text = [[self.movieListArray objectAtIndex:indexPath.row] country];
    float rating = [[[self.movieListArray objectAtIndex:indexPath.row] Rating] floatValue];
    if(rating > 6) ((UILabel *)[cell viewWithTag:103]).textColor = [UIColor blueColor];
        else ((UILabel *)[cell viewWithTag:103]).textColor = [UIColor grayColor];
        
    UIImageView* imgView = ((UIImageView *)[cell viewWithTag:100]);
    NSString* posterURL = [[self.movieListArray objectAtIndex:indexPath.row] posterURL];
        
    NSString* imageURL = [[NetController sharedClient] buildPosterURL:posterURL];
        
    [[NetController sharedClient] downloadImage:imgView fromPath:imageURL];
        
    
        
    }
 
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section==0){//Select Options Row
        [self setDataPickerArray:indexPath.row];
        
        
        [self showPicker:YES];
    }else{
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        
        DetailViewController *detailViewController =  (DetailViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"DetailViewController"];
        detailViewController.filmID = [[self.movieListArray objectAtIndex:indexPath.row] Id];
        [self presentViewController:detailViewController animated:YES completion:nil];
        
    }
}


#pragma mark Picker
- (void) initPicker{
    CGRect bounds = CGRectMake(0, 40, self.viewPickerSubView.bounds.size.width, self.viewPickerSubView.bounds.size.height - 40);
    
    self.optionsPicker = [[UIPickerView alloc]initWithFrame: bounds];
    
    self.optionsPicker.delegate = self;
    self.optionsPicker.dataSource = self;
    [self.viewPickerSubView addSubview:self.optionsPicker];
    self.pickerArray = genreArray;
    [self.optionsPicker reloadAllComponents];
}

- (void)setDataPickerArray:(NSInteger) row{
    self.pickerType = row;
    if(row == OPTION_CITY ) self.pickerArray = cityArray;
    if(row == OPTION_DATE ) self.pickerArray = self.dateArray;
    if(row == OPTION_GENRE) self.pickerArray = genreArray;
    [self.optionsPicker reloadAllComponents];
    
}

- (void)pickerView:(UIPickerView *)pV didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
   
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.pickerArray count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
  return [self.pickerArray objectAtIndex:row];
}

- (IBAction)buttonPickerYesAction:(id)sender {
    self.pickerSelectedRow = [self.optionsPicker selectedRowInComponent:0];
    
    [optionsKeys setObject: self.pickerArray[self.pickerSelectedRow] forKey: listOptionsKeys[self.pickerType]];
    
    [self filterMovieListby:self.pickerArray[self.pickerSelectedRow] inParam:self.pickerType];
    
    [self.tableView reloadData];
    [self showPicker:NO];
}

- (IBAction)buttonPickerNoAction:(id)sender {
    [self showPicker:NO];
}

-(void)showPicker:(BOOL)state{
    if(state == YES){
        [self.viewPicker setHidden:NO];
    }else{
        [self.viewPicker setHidden:YES];
    }
}

-(void) filterMovieListby:(NSString*)findStr inParam:(NSInteger)type {
    [self.movieListArray removeAllObjects];
    if([findStr rangeOfString:@"Все"].location!=NSNotFound)
       {
        [self.movieListArray addObjectsFromArray:self.movieListArrayAll];
        return;
       }
    for (MovieData* md in self.movieListArrayAll) {
        
        if((type==OPTION_GENRE)&&([md.genre rangeOfString:findStr options:NSCaseInsensitiveSearch].location!=NSNotFound)) [self.movieListArray addObject:md];
        if((type==OPTION_DATE)&&([findStr isEqualToString:md.premiereRU])) {
            [self.movieListArray addObject:md];
            }
    }
    
   
}
#pragma mark Segment Order

- (IBAction)segmentValueChanged:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    [self orderMovieListTo:selectedSegment];
   
}
-(void)orderMovieListTo:(NSInteger) selectedSegment{
     if (selectedSegment == ORDER_RATING) {
        [self.segmentOrder setSelectedSegmentIndex:ORDER_RATING];
        [self.movieListArray sortUsingComparator:^(MovieData* object1, MovieData* object2) {
            return [object2.rating caseInsensitiveCompare:object1.rating];
        }];
    }
    else{
        [self.segmentOrder setSelectedSegmentIndex:ORDER_ALPHA];
        [self.movieListArray sortUsingComparator:^(MovieData* object1, MovieData* object2) {
            return [object1.nameRu caseInsensitiveCompare:object2.nameRu];
        }];
    }
    [self.tableView reloadData];
}
@end
