//
//  FirstViewController.h
//  KinoPoisk
//
//  Created by user on 07.11.16.
//  Copyright © 2016 Ruabikov Sergey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieListViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>
@property (strong, nonatomic) IBOutlet UIView *viewPicker;
@property (strong, nonatomic) IBOutlet UIButton *buttonPickerYes;
@property (strong, nonatomic) IBOutlet UIButton *buttonPickerNo;
- (IBAction)buttonPickerYesAction:(id)sender;
- (IBAction)buttonPickerNoAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableArray* movieListArray;
@property (strong, nonatomic) NSMutableArray* movieListArrayAll;
@property (strong, nonatomic) NSMutableArray* dateArray;

@property (strong, nonatomic) NSMutableArray* optionsListArray;
@property (strong, nonatomic) NSArray* pickerArray;
@property (strong, nonatomic) NSString* pickerValue;
@property (nonatomic, assign) NSInteger pickerType;
@property (nonatomic, assign) NSInteger pickerSelectedRow;

@property (nonatomic, strong) UIPickerView *optionsPicker;
@property (strong, nonatomic) IBOutlet UIView *viewPickerSubView;

@property (strong, nonatomic) IBOutlet UIView *mainView;

@property (getter=getCityID,setter=setCityID:, strong, nonatomic) NSString* cityID;
@property (getter=getSelectDate,setter=setSelectDate:, strong, nonatomic) NSString* selectDate;


- (IBAction)segmentValueChanged:(id)sender;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentOrder;
@property (strong, nonatomic) UIRefreshControl* refreshControl;

@end

