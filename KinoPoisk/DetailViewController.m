//
//  DetailViewController.m
//  KinoPoisk
//
//  Created by Rsn on 10.11.16.
//  Copyright © 2016 Ruabikov Sergey. All rights reserved.
//

#import "DetailViewController.h"
#import "NetController.h"
#import "MovieData.h"
#import "UIImageView+AFNetworking.h"
#import "Utils.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

{
    NSArray* galleryImageViewArray;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDetailData];
    
    //[self loadTestMovieData];
    [self.viewWaiting setHidden:NO];
    [self loadDetailMovieData:self.filmID];
   
    
}
- (void)initDetailData{
    galleryImageViewArray = [[NSArray alloc]initWithObjects:self.imageViewScreen1,self.imageViewScreen2,self.imageViewScreen3,self.imageViewScreen4, nil];
    
    self.scrollViewGallery.contentInset = UIEdgeInsetsMake(0, 0, 0, 86*5);
   
}

- (void)loadDetailMovieData:(NSString*)filmID{
    //[self loadTestMovieData];
    
    NSMutableDictionary* dicParams = [[NSMutableDictionary alloc]init];
    [dicParams setObject: filmID forKey:@"filmID"];
    
    [[NetController sharedClient] request: GET_FILM parameters:dicParams success:^(NSURLSessionTask *task, id responseObject) {
        
        [self setupFilmData:responseObject];
        
    }failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [[Utils sharedClient]showMessage:@"Будут загружены тестовые данные." withTitle:@"Ошибка сервера. " inView:self.mainView];
        [self loadTestMovieData];
    }];

    
}
- (void)loadTestMovieData{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"getFilm" ofType:@"txt"];
    NSString *content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSError *err = nil;
    NSDictionary *dict =
    [NSJSONSerialization JSONObjectWithData:[content dataUsingEncoding:NSUTF8StringEncoding]
                                    options:NSJSONReadingMutableContainers
                                      error:&err];
    
    [self setupFilmData:dict];
}

- (void)setupFilmData:(NSDictionary*)film{
   
    self.labelMovieName.text = [film objectForKey:@"nameRU"];
    self.labelCountry.text   = [NSString stringWithFormat:@"%@(%@)",[film objectForKey:@"country"],[film objectForKey:@"year"] ];
   // self.labelYear.text      =
    self.labelGenre.text     = [film objectForKey:@"genre"];
    //self.labelGenre.text     = [film objectForKey:@"filmLength"];
    self.textViewDescription.text = [film objectForKey:@"description"];
    NSArray* galleryArray    = [film objectForKey:@"gallery"];
    NSArray* creatorArray    = [film objectForKey:@"creators"];
    NSArray* _array  = [creatorArray objectAtIndex:0] ;
    NSDictionary* dic = [_array objectAtIndex:0];
    self.labelDirector.text = [NSString stringWithFormat:@"Режиссер: %@", [dic objectForKey:@"nameRU"] ];
    
    @try{
    for(int i=0;i<4;i++){
       NSDictionary* dic = [galleryArray objectAtIndex:i];
       NSString* path = [dic objectForKey:@"preview"];
       [[NetController sharedClient] downloadImage:galleryImageViewArray[i] fromPath:path];
       }
    }
     @catch (NSException *e) {
     NSLog(@"FilmPreview:Index out of bound %@", e);
    }
    
    //NSArray *movieArray   = [dict objectForKey:@"previewFilms"];
    NSMutableDictionary* creatorKey = [[NSMutableDictionary alloc] init];
    [creatorKey setObject:self.labelDirector forKey:@"director"];
    [creatorKey setObject:self.labelProducer forKey:@"producer"];
    [creatorKey setObject:self.labelActors   forKey:@"actors"];
    self.labelActors.text = @"";
    self.labelDirector.text = @"";
    self.labelProducer.text = @"";
    
    for (NSArray* creatorSubArray in creatorArray) {
        for (NSDictionary* dicItem in creatorSubArray) {
            
            MovieData* mdata = [[MovieData alloc]init];
            mdata.nameRu     = [dicItem objectForKey:@"nameRU"];
            NSString* proKey = [dicItem objectForKey:@"professionKey"];
            if([proKey isEqualToString:@"director"]) self.labelDirector.text = [NSString stringWithFormat:@"%@,%@",self.labelDirector.text,mdata.nameRu ];
            if([proKey isEqualToString:@"producer"]) self.labelProducer.text = [NSString stringWithFormat:@"%@,%@",self.labelProducer.text,mdata.nameRu ];
            if([proKey isEqualToString:@"actor"]) self.labelActors.text = [NSString stringWithFormat:@"%@,%@",self.labelActors.text,mdata.nameRu ];
        
            //[self.movieListArray addObject:mdata];
        }
    }
    self.labelDirector.text = [self.labelDirector.text substringFromIndex: 1];
    self.labelProducer.text = [self.labelProducer.text substringFromIndex: 1];
    self.labelActors.text   = [self.labelActors.text substringFromIndex: 1];
    [self.labelActors sizeToFit];
    
    
    NSString* posterURL = [film objectForKey:@"posterURL"];
    
    NSString* imageURL = [[NetController sharedClient] buildPosterURL:posterURL];
    
    [[NetController sharedClient] downloadImage:self.imageViewMain fromPath:imageURL];

    [self.viewWaiting setHidden:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



- (IBAction)actionBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
