//
//  MovieDataController.h
//  KinoPoisk
//
//  Created by user on 07.11.16.
//  Copyright © 2016 Ruabikov Sergey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIImageView+AFNetworking.h"


#define RENT_MOVIES @"/getSoonFilms"
#define GET_FILM    @"/getFilm"




#define BaseURL @"http://api.kinopoisk.cf"
//#define BaseImgUrl @"http://st.kinopoisk.ru/images/film_iphone/iphone_463738.jpg"
//#define BaseImgUrl @"https://st.kinopoisk.ru/images/film_iphone/iphone360_463738.jpg"
#define BaseImgURL @"https://st.kinopoisk.ru/images/"

//https://st.kinopoisk.ru/images

//https://st.kp.yandex.net/images/film_iphone/iphone_463738.jpg?width=320


@interface NetController : NSObject

+ (instancetype)sharedClient;
- (void)getMovieDataFromCity:(NSString*)cityId;
- (void) request2:(NSString*) commandPath parameters:(NSDictionary*) parameters success:(void (^)(NSURLSessionDataTask *task, id responseObject))success ;
- (void) request:(NSString*) commandPath parameters:(NSDictionary*) parameters success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void(^)(NSURLSessionTask *operation, NSError *error))failure ;

-(NSString*)buildPosterURL:(NSString*)path;
- (void) downloadImage:(UIImageView*)imageView fromPath:(NSString*)imagePath;


@end
