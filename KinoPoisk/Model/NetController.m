//
//  MovieDataController.m
//  KinoPoisk
//
//  Created by user on 07.11.16.
//  Copyright © 2016 Ruabikov Sergey. All rights reserved.
//

#import "NetController.h"
#import "AFNetworking.h"
#import "MovieData.h"
#import "Utils.h"

//  getSoonFilms?date=[date]&cityID=[cityID] //Скоро в прокате
//  date - дата в формате DD.MM.YYYY
//  cityID - ID города


@implementation NetController

+ (instancetype)sharedClient {
    
    static NetController *_sharedClient = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedClient = [[NetController alloc] init];
    });
    
    return _sharedClient;
}



-(NSString*)buildPosterURL:(NSString*)path{
    
    NSRange range = [path rangeOfString:@"iphone" options:NSBackwardsSearch];
    NSMutableString *mPath = [NSMutableString stringWithString:path];
    [mPath insertString:@"360" atIndex:range.location + 6];
    
    return [NSString stringWithFormat:@"%@%@",BaseImgURL,mPath];
}

-(void) request:(NSString*) commandPath parameters:(NSDictionary*) parameters success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void(^)(NSURLSessionTask *operation, NSError *error))failure {
    NSString* URLString = [NSString stringWithFormat:@"%@%@",BaseURL,commandPath];
    
    [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:URLString parameters:parameters error:nil];
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setTimeoutInterval:3];
    [manager GET:URLString parameters:parameters progress:nil success:success failure:failure];
    
    
}


-(void) request2:(NSString*) commandPath parameters:(NSDictionary*) parameters success:(void (^)(NSURLSessionDataTask *task, id responseObject))success {
    NSString* URLString = [NSString stringWithFormat:@"%@%@",BaseURL,commandPath];
    
     [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:URLString parameters:parameters error:nil];
    
   
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:URLString parameters:parameters progress:nil success:success
    
    failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
    

}
- (void) downloadImage:(UIImageView*)imageView fromPath:(NSString*)imagePath{
    
  if([imagePath rangeOfString:@"http"].location == NSNotFound) imagePath = [NSString stringWithFormat:@"%@%@",BaseImgURL,imagePath];
    
  NSURLRequest* imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:imagePath]
                                              cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                          timeoutInterval:60];
    
__block UIImageView* imageViewWeak = imageView;
[imageView setImageWithURLRequest:imageRequest
                          placeholderImage:[UIImage imageNamed:@"movie64"]
                                   success:^(NSURLRequest *request , NSHTTPURLResponse *response , UIImage *image ){
                                       imageViewWeak.image = image;
                                   }
                                   failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                       NSLog(@"failed image loading: %@", error);
                                   }
 ];


}


@end
