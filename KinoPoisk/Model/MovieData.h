//
//  MovieData.h
//  KinoPoisk
//
//  Created by user on 07.11.16.
//  Copyright © 2016 Ruabikov Sergey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MovieData : NSObject
@property (getter = Id,        nonatomic,strong) NSString* Id;
@property (getter = NameRu,    nonatomic,strong) NSString* nameRu;
@property (getter = NameEn,    nonatomic,strong) NSString* nameEn;
@property (getter = Year,      nonatomic,strong) NSString* year;
@property (getter = Rating,    nonatomic,strong) NSString* rating;
@property (getter = posterURL, nonatomic,strong) NSString* posterURL;
@property (getter = filmLength,nonatomic,strong) NSString* filmLength;
@property (getter = country,   nonatomic,strong) NSString* country;
@property (getter = genre,     nonatomic,strong) NSString* genre;
@property (getter = videoURL,  nonatomic,strong) NSString* videoURL;
@property (getter = premiereRU,nonatomic,strong) NSString* premiereRU;


@end
