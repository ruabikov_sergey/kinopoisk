//
//  Utils.m
//  KinoPoisk
//
//  Created by user on 11.11.16.
//  Copyright © 2016 Ruabikov Sergey. All rights reserved.
//

#import "Utils.h"
#import "MBProgressHUD.h"

@implementation Utils

+ (instancetype)sharedClient {
    static Utils *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[Utils alloc] init];
    });
    return _sharedClient;
}



- (void)showMessage:(NSString*) message withTitle:(NSString*)title inView:(UIView*)view{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo: view animated:YES];
    hud.detailsLabel.text = message;
    hud.label.text = title;
    [hud hideAnimated:YES afterDelay:3.0f];
    
   }

-(NSString*)formatDate:(NSDate*)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd.MM.YYYY";
    NSString *strDate = [formatter stringFromDate:date];
    return strDate;
}


@end
