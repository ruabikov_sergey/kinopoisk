//
//  Utils.h
//  KinoPoisk
//
//  Created by user on 11.11.16.
//  Copyright © 2016 Ruabikov Sergey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utils : NSObject
+ (instancetype)sharedClient;
- (void)showMessage:(NSString*) message withTitle:(NSString*)title inView:(UIView*)view;
- (NSString*)formatDate:(NSDate*)date;
@end
