//
//  DetailViewController.h
//  KinoPoisk
//
//  Created by Rsn on 10.11.16.
//  Copyright © 2016 Ruabikov Sergey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *imageViewMain;
@property (strong, nonatomic) IBOutlet UILabel *labelMovieName;
@property (strong, nonatomic) IBOutlet UILabel *labelCountry;
@property (strong, nonatomic) IBOutlet UILabel *labelYear;
@property (strong, nonatomic) IBOutlet UILabel *labelGenre;
@property (strong, nonatomic) IBOutlet UITextView *textViewDescription;
@property (strong, nonatomic) IBOutlet UILabel *labelDirector;
@property (strong, nonatomic) IBOutlet UILabel *labelProducer;

@property (strong, nonatomic) IBOutlet UILabel *labelActors;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewScreen1;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewScreen2;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewScreen3;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewScreen4;
@property (strong, nonatomic) IBOutlet UINavigationBar *navigationBar;

@property (strong, nonatomic) NSString* filmID;

- (IBAction)actionBack:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) IBOutlet UIView *viewWaiting;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewGallery;

@end
