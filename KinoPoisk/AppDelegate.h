//
//  AppDelegate.h
//  KinoPoisk
//
//  Created by user on 07.11.16.
//  Copyright © 2016 Ruabikov Sergey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

